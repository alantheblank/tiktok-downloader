from random import randint
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from time import sleep
import requests


def parseURL(input: str = None) -> str:
    if(input):
        if(input[:5] == "https" and 'tiktok.com' in input):
            url = input.split("?", 1)[0]
            return url
    else:
        print("No URL provided")


def extractVideo(url: str) -> None:
    options = Options()
    options.headless = True
    page = webdriver.Firefox(options=options)
    page.get(url)
    sleep(1.5)
    element = page.find_element(By.TAG_NAME,"video")
    video = element.get_attribute("src")
    page.close()
    out = "output/" + chr(randint(48, 57)) + chr(randint(48, 57)) + chr(randint(48, 57)) + chr(randint(65, 90)) + \
          chr(randint(65, 90)) + chr(randint(65, 90)) + chr(randint(97, 122)) + \
          chr(randint(97, 122)) + chr(randint(97, 122)) + ".mp4"
    print(out)
    with open(out, "wb+") as f:
        r = requests.get(video)
        f.write(r.content)
        f.close()
        r.close()


if __name__ == "__main__":
    while True:
        url = parseURL(input=input("Please enter the url: "))
        extractVideo(url)
        sleep(0.5)
