# Tiktok Downloader

## Description
A python-based script for downloading a tiktok video from URL

## Installation
installation is fairly simple. NOTE: Requires Python 3.10+
1) download firefox and the [gekcodriver](https://github.com/mozilla/geckodriver/releases) (this is required as we use the firefox module of selenium)
2) install the requirements.txt
3) run main.py

## Usage
simply copy the url into the prompt from the code, it will continue asking for new videos until you end the script

## Contributing
I don't feel there's need to contribute to this code, I don't see where it can be improved upon but if you do feel free to make said change and submit a pull request!
